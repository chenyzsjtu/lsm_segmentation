function result = testLSM(Img, ini_mask)
    Img=double(Img(:,:,:));
    Img = WindowLeveling(Img, 2000,0);
    zoomedRawData=double(Img(:,:,:));
    Img_smooth=smooth3(zoomedRawData,'gaussian',[3 3 3],.8);  % smooth image by Gaussiin convolution

    timestep=40;  % time step
    mu=0.1/timestep;  % coefficient of the distance regularization term R(phi)
    iter_inner=5;
    iter_outer=5;
    lambda=10; % coefficient of the weighted length term L(phi)
    alfa=-4;  % coefficient of the weighted area term A(phi)
    epsilon=1.5; % papramater that specifies the width of the DiracDelta function
    
    
    % initialize LSF as binary step function
    c0=2;
    phi= double(-c0*(ini_mask==true) + c0*(ini_mask==false));
    
    assignin('base', 'phi', phi);
    




    potential=2;  
    if potential ==1
        potentialFunction = 'single-well';  % use single well potential p1(s)=0.5*(s-1)^2, which is good for region-based model 
    elseif potential == 2
        potentialFunction = 'double-well';  % use double-well potential in Eq. (16), which is good for both edge and region based models
    else
        potentialFunction = 'double-well';  % default choice of potential function
    end  

    % start level set evolution
    for n=1:iter_outer
        n
        phi = drlse_edge3D(phi, Img_smooth, lambda, mu, alfa, epsilon, timestep, iter_inner, potentialFunction);    
    end

    % refine the zero level contour by further level set evolution with alfa=0
    alfa=0;
    iter_refine = 1;
    phi = drlse_edge3D(phi, Img_smooth, lambda, mu, alfa, epsilon, timestep, iter_refine, potentialFunction);

    

    result = phi;
    


end