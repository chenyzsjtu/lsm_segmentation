function phi = drlse_edge3D(phi_0, Img, lambda,mu, alfa, epsilon, timestep, iter, potentialFunction)
%  Revised for 3D implementation.
%  Done by Arthur Chan. 20170328.  contact me: chenyzsjtu@outlook.com.
%  The algorithm used is based on the PPT attached.
if size(phi_0,3)==1
    error(message('dimensions wronged!'));
end


[Ix,Iy,Iz] = gradient(Img);
pixelValueMap1 = min(abs(Img-double(WindowLeveling(500,2000,0))),abs(Img-double(WindowLeveling(-1000,2000,0))+30));
pixelValueMap2 = min(abs(Img-double(WindowLeveling(0,2000,0))),abs(Img-double(WindowLeveling(-100,2000,0))));
valueMap = (pixelValueMap1-pixelValueMap2);
beta = 0.01;

phi=phi_0;

for k=1:iter
    
    [phi_x,phi_y,phi_z]=gradient(phi);
    s=sqrt(phi_x.^2 + phi_y.^2 + phi_z.^2);
    smallNumber=1e-10;  
    Nx=phi_x./(s+smallNumber); % add a small positive number to avoid division by zero
    Ny=phi_y./(s+smallNumber);
    Nz=phi_z./(s+smallNumber);
    curvature=div(Nx,Ny,Nz);
    
    %
    g_direc = Nx.*Ix + Ny.*Iy + Nz.*Iz;
    g = 1./(1+(abs(g_direc)).^1);
    [vx, vy, vz]=gradient(g);
    %
    
    if strcmp(potentialFunction,'single-well')
        distRegTerm = 6*del2(phi)-curvature;  % compute distance regularization term in equation (13) with the single-well potential p1.
    elseif strcmp(potentialFunction,'double-well');
        distRegTerm=distReg_p2(phi,phi_x,phi_y,phi_z,s);  % compute the distance regularization term in eqaution (13) with the double-well potential p2.
    else
        disp('Error: Wrong choice of potential function. Please input the string "single-well" or "double-well" in the drlse_edge function.');
    end
    
    diracPhi=Dirac(phi,epsilon);
    areaTerm=diracPhi.*g; % balloon/pressure force
    edgeTerm=diracPhi.*(vx.*Nx+vy.*Ny+vz.*Nz) + diracPhi.*g.*curvature;
    pixelTerm = diracPhi.*valueMap;
    
    %core function.
    phi=phi + timestep*(mu*distRegTerm + lambda*edgeTerm + alfa*areaTerm + beta*pixelTerm);
    
    c0=2;
    phi = (phi>c0)*c0 + (phi<=c0).*phi;
    phi = (phi<-c0)*(-c0) + (phi>=-c0).*phi;
end


function f = distReg_p2(phi,phi_x,phi_y,phi_z,s)
% compute the distance regularization term with the double-well potential p2 in eqaution (16)
a=(s>=0) & (s<=1);
b=(s>1);
ps=a.*sin(2*pi*s)/(2*pi)+b.*(s-1);  % compute first order derivative of the double-well potential p2 in eqaution (16)
dps=((s~=0).*ps+(s==0))./((s~=0).*s+(s==0));  % compute d_p(s)=p'(s)/s in equation (10). As s-->0, we have d_p(s)-->1 according to equation (18)
f = div(dps.*phi_x - phi_x, dps.*phi_y - phi_y, dps.*phi_z - phi_z) + 6*del2(phi);
%f = dps.*(1.5*del2(phi));
% when x is one-dimensional, 2*del2(x)= a(x+1)+a(x-1)-2*a(x)
%                            gradient(gradient(x)) = (a(x+2)+a(x-2)-2*a(x))/4



function f = div(nx,ny,nz)
[nxx,~,~]=gradient(nx);  
[~,nyy,~]=gradient(ny);
[~,~,nzz]=gradient(nz);
f=nxx+nyy+nzz;

function f = Dirac(x, sigma)
f=(1/2/sigma)*(1+cos(pi*x/sigma));
b = (x<=sigma) & (x>=-sigma);
f = f.*b;
