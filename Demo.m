
    
for i=1:zoomInRate
    %for test
%     i
     if i~=1
        continue;
     end
        

        
        %test
    testArea_y_range = 130:260;
    testArea_x_range = 110:240;
    testArea_z_range = 110:270;


    Img = rawData(testArea_y_range,testArea_x_range,testArea_z_range);
    
    Img=double(Img(:,:,:));
    Img = WindowLeveling(Img, 2000,0);
    zoomedRawData=double(Img(:,:,:));
    
    sigma=.8;    % scale parameter in Gaussian kernel
    G=fspecial('gaussian',3,sigma); % Caussian kernel
    Img_smooth=smooth3(zoomedRawData,'gaussian',[3 3 3],.8);  % smooth image by Gaussiin convolution
    [Ix,Iy,Iz]=gradient(Img_smooth);
    f=Ix.^2+Iy.^2+Iz.^2;
    zoomedEdgeMap=1./(1+f);  % edge indicator function.

    
    zoomedMask = colonInitialMask(testArea_y_range,testArea_x_range,testArea_z_range);


        %%Test
        %test = int8(zeros(x_max,y_max,z_max));
        %test(extended_x_min:extended_x_max,extended_y_min:extended_y_max,extended_z_min:extended_z_max) = 1;
        %nrrdwrite('test.nrrd', test, [1,1,1], [0,0,0], 'raw');


    zoomedResult = testLSM(Img_smooth, zoomedMask);
    bigger = zoomedResult>0;
    smaller = zoomedResult<=0;

    borderKernel = zeros(3,3,3);
    borderKernel(2,2,1) = 1;
    borderKernel(:,:,2) = [0,1,0;1,1,1;0,1,0];
    borderKernel(2,2,3) = 1;
    biggerShift = convn(double(bigger),borderKernel,'same')>0;
    border = uint8(smaller&biggerShift);
    
    imgDisplay = zoomedRawData(:,164-110,:);
    imgDisplay = permute(imgDisplay, [1,3,2]);
    phi = zoomedResult(:,164-110,:);
    phi = permute(phi,[1,3,2]);
    figure(2);
    imagesc(imgDisplay,[0, 255]); axis off; axis equal; colormap(gray); hold on;  contour(phi, [0,0], 'r');

    
    resultMask = uint8(zeros(size(rawData)));   
    resultMask(testArea_y_range,testArea_x_range,testArea_z_range)=smaller;    
    nrrdwrite('resultMask.nrrd', resultMask,[1 1 1], [0 0 0], 'raw');
        
    

end
