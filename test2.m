imgData = [-1000,0,100,500];
level = 0;
window = 2000;

minimum = level - window/2.0
maximum = level + window/2.0
output = max(minimum, min(maximum, imgData))
output = uint16((output-minimum)/window*255)