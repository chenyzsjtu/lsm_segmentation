function [image ] = GeneratePhantom()
%GENERATEPHANTOM Summary of this function goes here
%   Detailed explanation goes here
    length = 256;
    height = 256;
    image = uint8(zeros(length,height));

    numTri = 1;
    num = 0;
    while num<numTri
        [px1,py1] = randomPoint(length, height);
    	[px2,py2] = randomPoint(length, height);
        [px3,py3] = randomPoint(length, height);
        if (px1==px2&&py1==py2)||(px1==px3&&py1==py3)||(px2==px3&&py2==py3)
            continue;
        end
        if (py2-py1)*(px3-px1)==(py3-py1)*(px2-px1)
            if ~((px2==px1)&&(py3==py1) || (px3==px1)||(py2==py1))
                continue;
            end
        end
        num = num+1;
        image = PaintLine(image,px1,py1,px2,py2);
        image = PaintLine(image,px1,py1,px3,py3);
        image = PaintLine(image,px2,py2,px3,py3);
        
    end
    

end


function [px,py] = randomPoint(length,height)
    px = uint16(random('Uniform',1,length));
    py = uint16(random('Uniform',1,height));

end

function output = PaintLine(image,px1,py1,px2,py2)
    if px1>px2
        px3=px1;px1=px2;px2=px3;
        py3=py1;py1=py2;py2=py3;
    end
    
    output = image;
    blackness = uint16(random('Uniform',1,256));
    if px1==px2
       output(px1,py1:py2) = blackness;
    else
       [xx,yy] = meshgrid(1:size(output,1),1:size(output,2));
       xx = xx .* (xx>=px1 .* xx<=px2)
       
    end
    
end



