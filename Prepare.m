clear all;
close all;

%% Path Init.
%addpath([pwd,'\..\MedicalImageDataIO']);


%% The following variable must exist before script is executed.
urlRawData = 'data.raw';
ulrMaskData = 'result.raw';

%% Script
%Import image data.
fileHandle = fopen(urlRawData);
rawData = fread(fileHandle,'int16=>int16');
x_max = 512; y_max=512; z_max=size(rawData,1)/x_max/y_max;
rawData = reshape(rawData, [x_max,y_max,z_max]);  
rawData = permute(rawData, [2,1,3]);
fclose(fileHandle);

%import initial mask data.
fileHandle = fopen(ulrMaskData);
colonInitialMask = fread(fileHandle,'uint8=>uint8');
colonInitialMask = reshape(colonInitialMask, [x_max,y_max,z_max]);
colonInitialMask = permute(colonInitialMask, [2,1,3]);
fclose(fileHandle);

   
colonInitialMask = colonInitialMask==1;

distMap = bwdistgeodesic(colonInitialMask, [327+270*x_max+77*x_max*y_max]);


maxLen = max(distMap(:));
zoomInRate = 10;
    