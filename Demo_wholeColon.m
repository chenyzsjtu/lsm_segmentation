resultMask = uint8(zeros(size(rawData)));  
    
for i=1:zoomInRate
        
    dist_min = maxLen/zoomInRate*(i-1);
    dist_max = maxLen/zoomInRate*i;

    [x,y,z] = meshgrid(1:x_max,1:y_max,1:z_max);
    targetArea = (distMap>dist_min).*(distMap<dist_max);
    x = x .* targetArea;
    targetArea_x_max = max(x(:));
    x = x + (x==0)*targetArea_x_max;
    targetArea_x_min = min(x(:));

    y = y .* targetArea;
    targetArea_y_max = max(y(:));
    y = y + (y==0)*targetArea_y_max;
    targetArea_y_min = min(y(:));

    z = z .* targetArea;
    targetArea_z_max = max(z(:));
    z = z + (z==0)*targetArea_z_max;
    targetArea_z_min = min(z(:));
    
        
    EXTENDED_AREA_LENGTH = 30;  %Hyper parameter.
    reserve_len = 20;
    subtract_len = EXTENDED_AREA_LENGTH - reserve_len;
    extended_x_max = targetArea_x_max + EXTENDED_AREA_LENGTH;   %Area to be processed
    extended_x_min = targetArea_x_min - EXTENDED_AREA_LENGTH;
    extended_y_max = targetArea_y_max + EXTENDED_AREA_LENGTH;
    extended_y_min = targetArea_y_min - EXTENDED_AREA_LENGTH;
    extended_z_max = targetArea_z_max + EXTENDED_AREA_LENGTH;
    extended_z_min = targetArea_z_min - EXTENDED_AREA_LENGTH;
    

    reserve_x_max = targetArea_x_max + reserve_len;  %Area to be reserved after being processed.
    reserve_x_min = targetArea_x_min - reserve_len;
    reserve_y_max = targetArea_y_max + reserve_len;
    reserve_y_min = targetArea_y_min - reserve_len;
    reserve_z_max = targetArea_z_max + reserve_len;
    reserve_z_min = targetArea_z_min - reserve_len;
    
    
    if extended_z_max > z_max
        extended_z_max = z_max;
        reserve_z_max = z_max;
        sub_z_max = extended_z_max-extended_z_min+1;
    else
        sub_z_max = extended_z_max-extended_z_min-subtract_len+1;
    end
    if extended_z_min<1
        extended_z_min = 1;
        reserve_z_min = 1;
        sub_z_min = 1;
    else
        sub_z_min = subtract_len+1;
    end

    
    sub_x_max = extended_x_max-extended_x_min-subtract_len+1;
    sub_x_min = subtract_len+1;
    sub_y_max = extended_y_max-extended_y_min-subtract_len+1;
    sub_y_min = subtract_len+1;

    
        %test
    testArea_y_range = extended_y_min:extended_y_max;
    testArea_x_range = extended_x_min:extended_x_max;
    testArea_z_range = extended_z_min:extended_z_max;
    

    Img = rawData(testArea_y_range,testArea_x_range,testArea_z_range);
    
    zoomedMask = colonInitialMask(testArea_y_range,testArea_x_range,testArea_z_range);


    % Core function.
    zoomedResult = testLSM(Img, zoomedMask);
    
   
    
    bigger = zoomedResult>0;
    smaller = zoomedResult<=0;

    borderKernel = zeros(3,3,3);
    borderKernel(2,2,1) = 1;
    borderKernel(:,:,2) = [0,1,0;1,1,1;0,1,0];
    borderKernel(2,2,3) = 1;
    biggerShift = convn(double(bigger),borderKernel,'same')>0;
    border = uint8(smaller&biggerShift);
    
%     imgDisplay = zoomedRawData(:,164-110,:);
%     imgDisplay = permute(imgDisplay, [1,3,2]);
%     phi = zoomedResult(:,164-110,:);
%     phi = permute(phi,[1,3,2]);
%     figure(2);
%     imagesc(imgDisplay,[0, 255]); axis off; axis equal; colormap(gray); hold on;  contour(phi, [0,0], 'r');


    
    resultMask(reserve_y_min:reserve_y_max, reserve_x_min:reserve_x_max, reserve_z_min:reserve_z_max) = ...
        smaller(sub_y_min:sub_y_max, sub_x_min:sub_x_max, sub_z_min:sub_z_max);
    

end


nrrdwrite('resultMask.nrrd', resultMask,[1 1 1], [0 0 0], 'raw');
