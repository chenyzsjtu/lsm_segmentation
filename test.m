testArea_y_range = 130:300;
%testArea_y_range = 1:512;
testArea_x_range = 167;
testArea_z_range = 110:300;
%testArea_z_range = 1:566;
Img = rawData(testArea_y_range,testArea_x_range,testArea_z_range);
Img=double(Img(:,:,:));
Img = WindowLeveling(Img, 4095,1024);
Img=double(Img(:,:,:));
Img = permute(Img,[3 1 2]);

%Img = Img(1:4:size(Img,1),1:4:size(Img,2));
Img = interp2(Img,1);


% parameter setting
timestep=20;  % time step
mu=0.2/timestep;  % coefficient of the distance regularization term R(phi)
iter_inner=10;
iter_outer=10;
lambda=5; % coefficient of the weighted length term L(phi)
alfa=-0;  % coefficient of the weighted area term A(phi)
epsilon=1.5; % papramater that specifies the width of the DiracDelta function

sigma=0.8;    % scale parameter in Gaussian kernel
G=fspecial('gaussian',15,sigma); % Caussian kernel
Img_smooth=conv2(Img,G,'same');  % smooth image by Gaussiin convolution
S = [-1,0,1;-2,0,2;-1,0,1]/4;
Ix = conv2(Img_smooth,S,'same');
Iy = conv2(Img_smooth,S','same');
%[Ix,Iy]=gradient(Img_smooth);
f=Ix.^2+Iy.^2;
g=1./(1+f);  % edge indicator function.
figure(5);
imshow(Img,[]);
% initialize LSF as binary step function
c0=2;
phi0 = colonInitialMask(testArea_y_range,testArea_x_range,testArea_z_range);
phi0 = permute(phi0,[3 1 2]);
%phi0 = phi0(1:4:size(phi0,1),1:4:size(phi0,2));

phi = (phi0==1)*-c0 + (phi0==0)*c0;
phi = interp2(phi,1);


imgDisplay = Img;%WindowLeveling(Img, 4095,1024);
figure(2);
imagesc(imgDisplay); axis off; axis equal; colormap(gray); hold on;  contour(phi, [0,0], 'r');
title('Initial zero level contour');
%pause(0.5);

potential=2;  
if potential ==1
    potentialFunction = 'single-well';  % use single well potential p1(s)=0.5*(s-1)^2, which is good for region-based model 
elseif potential == 2
    potentialFunction = 'double-well';  % use double-well potential in Eq. (16), which is good for both edge and region based models
else
    potentialFunction = 'double-well';  % default choice of potential function
end  

% start level set evolution
for n=1:iter_outer
    phi = drlse_edge(phi, Ix,Iy,g,lambda, mu, alfa, epsilon, timestep, iter_inner, potentialFunction);    
    if mod(n,2)==0
        figure(2);
        imagesc(Img,[0, 256-1]); axis off; axis equal; colormap(gray); hold on;  contour(phi, [0,0], 'r');
    end
end

% refine the zero level contour by further level set evolution with alfa=0
alfa=0;
iter_refine = 1;
phi = drlse_edge(phi, Ix,Iy,g, lambda, mu, alfa, epsilon, timestep, iter_refine, potentialFunction);

finalLSF=phi;
figure(2);
imagesc(imgDisplay,[0, 256-1]); axis off; axis equal; colormap(gray); hold on;  contour(phi, [0,0], 'r');
hold on;  contour(phi, [0,0], 'r');
str=['Final zero level contour, ', num2str(iter_outer*iter_inner+iter_refine), ' iterations'];
title(str);


[nrow, ncol]=size(Img);
axis([1 ncol 1 nrow -5 5]);
set(gca,'ZTick',[-3:1:3]);
set(gca,'FontSize',14);
fmin = min(phi(:))
fmax = max(phi(:))

