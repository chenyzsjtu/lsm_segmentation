function [ output ] = WindowLeveling( imgData, window, level )
%WINDOWLEVELING Summary of this function goes here
%   Detailed explanation goes here
minimum = level - window/2.0;
maximum = level + window/2.0;
output = max(minimum, min(maximum, imgData));
output = uint16((output-minimum)/window*(256-1));

end

